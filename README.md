#Lab. 5: Estructuras de decisión

En casi todas las instancias en que queremos resolver un problema hay una o más opciones que dependen  de si se cumplen o no ciertas condiciones. Los programas de computadoras se construyen para resolver problemas y, por lo tanto, deben tener una estructura que permita tomar decisiones. En C++ las  instrucciones de decisión (o condicionales) se estructuran utilizando `if`, `else`, `else if` o `switch`. En la experiencia de laboratorio de hoy practicaremos el uso de algunas de estas instrucciones completando el diseño de una aplicación que determina cuan "fuerte" es una contraseña de acceso ("password"). También practicarás el uso de algunos comandos básicos para darle formato a la información que despliegas en el terminal.


#Objetivos:

Al finalizar la experiencia de laboratorio de hoy los estudiantes habrán reforzado su conocimiento sobre estructuras de decisión y practicado su uso. También habrán practicado el darle formato a los datos de salida de un programa en C++.




#Pre-Lab:

Antes de llegar al laboratorio cada estudiante debe:

1. haber repasado los conceptos relacionados a estructuras de decisión.

2. haber estudiado los conceptos e instrucciones para la sesión de laboratorio.

3. haber tomado el [quiz Pre-Lab 5](http://moodle.ccom.uprrp.edu/course/modedit.php?update=6699&return=0) (recuerda que el quiz Pre-Lab puede tener conceptos explicados en las instrucciones del laboratorio).


#Sesión de laboratorio:

Tu tarea en esta experiencia de laboratorio es ayudar a diseñar una aplicación que mida la fortaleza de las contraseñas de acceso ("password strength") que será una versión simplificada de la aplicación en http://www.passwordmeter.com/. La fortaleza de la contraseña se cuantificará otorgando puntos por utilizar "buenas" técnicas de selección de contraseñas (como utilizar símbolos y letras) y restando puntos por utilizar "malos" hábitos en las contraseñas (como utlizar solo letras minúsculas o símbolos consecutivos de un mismo tipo). Las siguientes tablas resumen los valores añadidos y sustraidos para varias características en las contraseñas. Tu programa analizará la contraseña dada por el usuario y usará los criterios en las tablas para computar una puntuación para la fortaleza de la contraseña.


## Asignando puntuación a las contraseñas

## Sumas

|      | Categoría                        | Puntos                                   | Notas                                                |
| :--- | :-------------------------------- | :-------------------------------------: | ----------------------------------------------------: |
| 1.   | Número de caracteres             | $4\times len$                                    | *len* es el largo de la contrasena                        |
| 2.   | Letras mayúsculas                | ![](http://i.imgur.com/Pp21yYJ.gif)     | *n* es el número de letras mayúsculas                |
| 3.   | Letras minúsculas                | ![](http://i.imgur.com/0GLIVSG.gif)     | *n* es el número de letras minúsculas                |
| 4.   | Dígitos                          | ![](http://i.imgur.com/Q6bkVob.gif)     | *n* es el número de dígitos                          |
| 5.   | Símbolos                         | ![](http://i.imgur.com/KaZrZQI.gif)     | *n* es el número de símbolos                         |
| 6.   | Dígitos o símbolos en el medio   | $2n$                                    | *n* es el número de dígitos y símbolos en el medio   |
| 7.   | Requisitos                       | $2n$                                    | *n* es el número de requisitos que se cumplen        |



Lo que sigue son algunos detalles adicionales y ejemplos de los criterios para **sumas**.

1. **Número de caracteres**: este es el criterio más directo. La puntuación es 4 veces el largo de la contraseña. Por ejemplo, `"ab453"` tendría puntuación de $4 \times 5 = 20$.

2. **Letras mayúsculas** La puntuación es $2 \(len - n\)$ si la contraseña consiste de una mezcla de letras mayúsculas **Y** al menos otro tipo de caracter (minúscula, dígitos, símbolos). De lo contrario, la puntuación es $0$. Por ejemplo,

    a. la puntuación para `"ab453"` sería $0$ ya que no tiene letras mayúsculas.

    b. la puntuación para `"ALGO"` sería $0$ porque **solo** contiene letras mayúsculas.

    c. la puntuación para `"SANC8in"` sería $2 \(7-4\) = 6$ porque la contraseña es de largo $7$, contiene $4$ letras mayúsculas, y contiene caracteres de otro tipo.

3. **Letras minúsculas** La puntuación es $2 \(len - n\)$ si la contraseña consiste de una mezcla de letras minúsculas **Y** al menos otro tipo de caracter (mayúscula, dígitos, símbolos). De lo contrario, la puntuación es $0$. Por ejemplo,

    a. la puntuación para `"ab453"` sería $2 \(5-2\) = 6$ porque la contraseña es de largo $5$, contiene $2$ letras minúsculas, y contiene caracteres de otro tipo.

    b. la puntuación para `"ALGO"` sería $0$ porque no contiene letras minúsculas.

    c. la puntuación para `"sancochin"`  sería $0$ porque contiene **solo** letras minúsculas.

4. **Dígitos** La puntuación es $4n$ si la contraseña consiste de una mezcla de dígitos **Y** al menos otro tipo de caracater (minúscula, mayúscula, símbolos). De otro modo la puntuación es $0$. Por ejemplo,

  a. la puntuación para `"ab453"` sería $4 \times 3 = 12$ porque la contraseña contiene $3$ dígitos y contiene caracteres de otro tipo.

  b. la puntuación para `"ALGO"` sería $0$ porque no dígitos.

  c. la puntuación para `801145555`  sería $0$ porque contiene **solo** dígitos.

5. **Símbolos** La puntuación es $6n$ si la contraseña consiste de una mezcla de símbolos. De otro modo la puntuación es $0$. Por ejemplo,

  a. la puntuación para `"ab453"` sería $0$ porque no contiene símbolos.

  b. la puntuación para `"ALGO!!"` sería $6 \times 2$ porque contiene $2$ símbolos y contiene otros tipos de caracteres.

  c. la puntuación para `---><&&`  sería $6 \times 7 = 42$ porque contiene $7$ símbolos. Nota que en el caso de símbolos, se otorga puntuación incluso cuando no hay otro tipo de caracteres.

6. **Dígitos o símbolos en el medio** La puntuación es $2n$ si la contraseña contiene símbolos o dígitos que no están en la primera o última posición. Por ejemplo,

  a. la puntuación para `"ab453"` sería $2 \times 2 = 4$ porque contiene dos dígitos que no están en la primera o última posición, estos son `4` y `5`.

  b. la puntuación para `"ALGO!"` sería $0$ porque no contiene dígitos ni símbolos en el medio, el único símbolo está al final.

  c. la puntuación para `S&c8i7o!`  sería $2 \times 3 = 6$ porque contiene $3$ símbolos o dígitos en el medio, estos son `&`, `8`, y `7`.

7. **Requisitos**: La puntuación es $2n$, donde $n$ es el número de *requisitos* que se cumplen. Los requisitos son:

    a. La contraseña debe tener 8 o más caracteres de largo.

    b. Debe contener *al menos 3* de los siguientes renglones
        - Letras mayúsculas
        - Letras minúsculas
        - Números
        - Símbolos

    Se otorga  $2n$ solo si el criterio del largo **Y** 3 o 4 de los otros criterios se cumplen. Por ejemplo,

      a. la puntuación para `"ab453"` sería $0$ porque el criterio del largo no se cumple.

      b. la puntuación para `"abABCDEF"` sería $0$ debido a que, a pesar de que se cumple el criterio del largo, solo 2 de los 4 otros criterios se cumplen (mayúsculas y minúsculas).

      c. la puntuación para `"abAB99!!"` sería $2 \times 5 = 10$ debido a que cumple la condición del largo y también los otros 4 criterios.


## Restas


|      | Categoría                         | Puntos                                   | Notas                                                                   |
| :--- | :-------------------------------- | :-------------------------------------: | ----------------------------------------------------:                   |
| 1.   | Solo letras                       | ![](http://i.imgur.com/HltS4m3.gif)     | *len* es el largo de la contrasena                                           |
| 2.   | Solo números                      | ![](http://i.imgur.com/rulD7tG.gif)     | *len* es el largo de la contrasena                                           |
| 3.   | Letras mayúsculas consecutivas    | $-2n$                                   | *n* es el número de letras mayúsculas que siguen a otra letra mayúscula |
| 4.   | Letras minúsculas consecutivas    | $-2n$                                   | *n* es el número de letras minúsculas que siguen a otra letra minúscula                                                                     |
| 5.   | Dígitos consecutivos              | $-2n$                                   | *n* es el número de dígitos que siguen a otro dígito                     |


Lo que sigue son algunos detalles adicionales y ejemplos de los criterios para **restas**.

1. **Letras solamente**: La puntuación es $-len$ para una contrseña que consista solo de letras, de otro modo obtiene $0$. Por ejemplo, 

    a. la puntuación para `"ab453"` sería $0$ ya que contiene solo letras y números

    b. la puntuación para `"Barrunto"` sería $-8$ ya que consiste solo de letras y su largo es $8$.

2. **Dígitos solamente**: La puntuación es $-len$ para una contraseña que consista solo de dígitos, de otro modo obtiene $0$. Por ejemplo, 

    a. la puntuación para `"ab453"` sería $0$ ya que contiene solo letras y números

    b. la puntuación para `987987987` sería $-9$ ya que consiste solo de dígitos y su largo es $9$.

3. **Letras mayúsculas consecutivas**: La puntuación es $-2n$ donde *n* es el número de letras mayúsculas que siguen a otra letra mayúscula. Por ejemplo,

    a. la puntuación para `"DB453"` sería $-2 \times 1 = -2$ ya que solo contiene una letras mayúscula (`B`) que sigue a otra letra mayúscula.

    b. la puntuación para `"TNS1PBMA"` sería $-2 \times 5 = -10$ ya que contiene 5 letras mayúsculas (`N`, `S`, `B`, `M`, `A`) que siguen a otra letra mayúscula.

4. **Letras minúsculas consecutivas**: Igual que el criterio #3 pero para letras minúsculas.

5. **Dígitos consecutivos**: Igual que el criterio #3 pero para dígitos.


## Usando manipuladores para desplegar resultados *bonitos*

El archivo de encabezado de C++ `iomanip` contiene algunos *manipuladores* que nos ayudan a controlar como se desplegan los resultados del comando `cout`. La siguiente tabla contiene algunos de los más utilizados:

|     Manipulador      |                                                                            Descripción                                                                            |
|:----------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------:|
|setw(int n)          | Establece el ancho de la próxima cadena de caracteres como *n*. Por defecto, la cadena se alineará a la derecha dentro de ese campo.                              |
|left                 | Alineamiento de izquierda dentro del ancho del campo.                                                                                                             |
|right                | Alineamiento de derecha dentro del ancho del campo.                                                                                                               |
|setprecission(int n) | Establece la precisión decimal que se usará para darle forma a los valores de punto flotante ("floating-point values") en las operaciones de despliegue de datos. |
|fixed                | Escribe los valores de punto flotante en notación de punto fijo.                                                                                                  |
|scientific           | Escribe los valores de punto flotante en notación científica.                                                                                                                                                                  |

Lo que sigue es un ejemplo de cómo podemos producir una *tabla* usando `cout` y manipuladores.

**Ejemplo**

```cpp
  cout << left << setw(25) << "Criterio" ;
  cout << left << setw(10) << "Conteo";
  cout << left << setw(10) << "Puntacion" << endl;

  cout << left << setw(25) << "Numero de caracteres" ;
  cout << left << setw(10) << ctrChars;
  cout << left << setw(10) << scoreCountChars << endl;
```

Se desplegaría algo como esto:

```cpp
Criterio                 Conteo    Puntuacion     
Numero de caracteres     9         36  
```

Claro que, como tú eres un gran programador, habrás notado que hay un patrón repetitivo y construirías una función como la que sigue ...


```cpp
void outputLine(const string &criteria,
                const string &count,
                const string &score)
{
    cout << left << setw(25) << criteria ;
    cout << left << setw(10) << count;
    cout << left << setw(10) << score << endl;
}
```


... y la invocarías así:

```cpp
  outputLine("Criterio", "Conteo", "Puntuacion");
  outputLine("Numero de caracteres", to_string(ctrChars), 
             to_string(scoreCountChars));

```


##Ejercicio 

En este ejercicio practicaremos el uso de expresiones matemáticas y estructuras condicionales para computar la puntuación de fortaleza de una contraseña combinando las puntaciones de los criterios individuales.


###Instrucciones

1.	Ve a la pantalla de terminal y escribe el comando `git clone https://bitbucket.org/eip-uprrp/Lab05-Conditionals.git` para descargar la carpeta `Lab05-Conditionals` a tu computadora.

2.	Marca doble "click" en el archivo `passwordStrengthCL.pro` para cargar este proyecto a Qt. 

3. El proyecto consiste de varios archivos:

      * `psfunctions.cpp` : contiene las implantaciones de las funciones que tu programa va a invocar para calcular la puntuación de la fortaleza total de la contrasena. **No tienes que cambiar nada del código en este archivo ni en el archivo `psfunctions.h`** Simplemente invocarás desde `main` las funciones contenidas en ellos, según sea necesario. Hay funciones que no necesitarás invocar.
      * `psfunctions.h` : contiene los prototipos de las funciones definidas en `psfunctions.cpp`.
      *  `psfunctionTests.cpp` y `psfunctionTests.h` : contienen las implantaciones de las funciones y prototipos para algunas pruebas que verifican si las funciones en `psfunctions.cpp`  trabajan como se supone. **No tienes que cambiar nada dentro de este archivo.**

4. El código que te proveemos contiene las funciones que computan la puntuación dada por la mayoría de los criterios. [Aquí](http://ccom.uprrp.edu/~rarce/ccom3033f14/documentation/passwordStrengthCL/psfunctions_8cpp.html) hay una lista y descripción de las funciones. Tu tarea es utilizar expresiones matemáticas y estructuras condicionales para computar la puntuación de fortaleza de una contraseña combinando las puntaciones de los criterios individuales.

    El dato de entrada ("input") de tu programa es la contraseña del usuario ("user's password"). Los datos de salida ("output") será un *informe* que contenga los distintos criterios, el conteo para cada criterio, y la puntuación individual para ese criterio. La puntuación total será la suma de todas los puntos (sumas y restas) de los criterios individuales.

    Basado en la puntuación total, el programa debe clasificar la fortaleza de la contraseña como:

| Puntación total |  Fortaleza  |
|-----------------|-------------|
| [0,20)          | Bien débil  |
| [20,40)         | Débil       |
| [40,60)         | Buena       |
| [60,80)         | Fuerte      |
| [80,100)        | Bien fuerte |



    **Ejemplo:**

```
Contrasena: caba77o

Criterio                      Conteo    Puntuacion

Numero de caracteres          7         28
Letras mayusculas             0         0
Letras minusculas             5         4
Digitos                       2         8
Simbolos                      0         0
Digitos/Simbolos en el medio  2         4
Requisitos                    0         0

Letras solamente              0         0
Numeros solamente             0         0
Mayusculas consecutivas       0         0
Minusculas consecutivas       3        -6
Digitos consecutivos          1        -2
==========================================
Total                                  36

La foraleza de tu contrasena es: DEBIL

```


**Ejemplo**


```
Contrasena: S1nf@nia!

Criterio                      Conteo    Puntuacion

Numero de caracteres          9         36
Letras mayusculas             1         16
Letras minusculas             5         8
Digitos                       1         4
Simbolos                      2         12
Digitos/Simbolos en el medio  2         4
Requisitos                    5         10

Letras solamente              0         0
Numeros solamente             0         0
Mayusculas consecutivas       0         0
Minusculas consecutivas       3        -6
Digitos consecutivos          0         0
==========================================
Total                                  84

La foraleza de tu contrasena es: BIEN FUERTE

```

En el ejemplo anterior, el conteo de los **requisitos** es 5 porque `"S1nf@nia!"` cumple con el criterio de largo y también contiene mayúsculas, minúsculas, números y símbolos.

Copia el código que utilizaste (incluyendo declaraciones de variables, `cin` y `cout`) para determinar la fortaleza de la contraseña y crear la tabla con los resultados en la sección correspondiente de la [página de Entregas del Lab 5](http://moodle.ccom.uprrp.edu/mod/quiz/view.php?id=6700).

**NOTA** Para que el código se vea bien en la caja en donde pegas tu código, selecciona la opción `Preformatted` en el menú en donde dice `Paragraph`.

