#include "psfunctions.h"



///
/// \brief countCharsOfType - given a string will return the number of characters
/// of a certain type.
/// \param st - string to analyze
/// \param filterFunction - a function pointer to a function that returns 1 
/// when a character of the type is given. For example, if function is islower,
/// then countCharsOfType will count lowercase characters.
/// \param fromIdx - index from where to start count
/// \param toIdx - last index for count 
///

unsigned int countCharsOfType(const string &st,
    int (* filterFunction)(int args) ,
    int fromIdx = 0, int toIdx = -1) {

    unsigned int ctr = 0;
    unsigned int len = st.length();

    // set the last index, -1 is the default value, 
    // which will be interpreted as the last index.
    if (toIdx == -1) toIdx = len - 1;

    //cout << "fromIdx: " << fromIdx << " toIdx:" << toIdx << endl;

    // if length or indices are invalid return a 0
    if ( len == 0 || fromIdx < 0 || fromIdx > static_cast<int>(len - 1) || 
         fromIdx > toIdx || toIdx < 0 || toIdx > static_cast<int>(len - 1) )
        return 0;

    for(int i = fromIdx; i <= toIdx; i++) {
        if ( filterFunction( st[i]) ) 
            ctr++;
    }
    return ctr;
}

///
/// \brief countUppercase - given a string will return the number of uppercase 
/// characters.
/// \param st - string to analyze
///

unsigned int countUppercase(const string &st) {
    return countCharsOfType(st, isupper);
}

///
/// \brief countLowercase - given a string will return the number of lowercase 
/// characters.
/// \param st - string to analyze
///

unsigned int countLowercase(const string &st) {
    return countCharsOfType(st, islower);
}

///
/// \brief isSymbol - returns 1 if the passed argument is a symbol.
/// \param c - the character to be analyzed
///

int isSymbol(int c) {
    return ( !islower(c) && !isupper(c) && !isdigit(c) );
}

///
/// \brief isDigitOrSymbol - returns 1 if the passed argument is a digit or symbol.
/// \param c - the character to be analyzed
///

int isDigitOrSymbol(int c) {
    return ( isdigit(c) || isSymbol(c) );
}

///
/// \brief countDigits - given a string will return the number of digits.
/// \param st - string to analyze
///

unsigned int countDigits(const string &st) {
    return countCharsOfType(st, isdigit);
}

///
/// \brief countSymbols - given a string will return the number of symbols.
/// \param st - string to analyze
///

unsigned int countSymbols(const string &st) {
    return countCharsOfType(st, isSymbol);
}

///
/// \brief toUpperString - returns an uppercase version of the received string.
/// \param st - string to convert
///

string toUpperString(const string &st) {
    string res = "";
    for (unsigned int i = 0; i < st.length(); i++)
        res.push_back(toupper(st[i]));
    return res;
}

///
/// \brief middleDigitsOrSymbols - returns the number of digits and symbols 
/// that are not the first or last characters of the received string.
/// \param st - string to analyze
///

unsigned int middleDigitsOrSymbols(const string &st) {
    return countCharsOfType(st, isDigitOrSymbol, 1, static_cast<int>(st.length()) - 2);
}


///
/// \brief countConsecutive - given a string will return the number of characters
/// of a certain type that follow a character of that same type.
/// \param st - string to analyze
/// \param filterFunction - a function pointer to a function that returns 1 
/// when a character of the type is given. For example, if function is islower,
/// then countConsecutive will count consecutive lowercase characters.
///

unsigned int countConsecutive(const string &st,
    int (* filterFunction)(int args) ) {

    // prevSameType will be made true whenever we find a caracter
    // of the type we are looking.
    bool prevSameType = false;
    unsigned int ctr  = 0;

    for(unsigned int i = 0; i < st.length(); i++) {
        if ( filterFunction( st[i]) ) {
            if (prevSameType) ctr++;
            prevSameType = true;
        }
        else
            prevSameType = false;
    }
    return ctr;
}


///
/// \brief consecUppercase - given a string will return the number of 
/// uppercase characters that follow a character of that same type.
/// \param st - string to analyze
///

unsigned int consecUppercase(const string &st) {
    return countConsecutive(st, isupper);
}

///
/// \brief consecLowercase - given a string will return the number of 
/// lowercase characters that follow a character of that same type.
/// \param st - string to analyze
///

unsigned int consecLowercase(const string &st) {
    return countConsecutive(st, islower);
}

///
/// \brief consecDigits - given a string will return the number of 
/// digits that follow a digit.
/// \param st - string to analyze
///

unsigned int consecDigits(const string &st) {
    return countConsecutive(st, isdigit);
}

