#ifndef PSFUNCTIONTESTS_H
#define PSFUNCTIONTESTS_H

#include <cassert>
#include "psfunctions.h"
void test_countUppercase();

void test_countLowercase();

void test_countDigits();

void test_countSymbols();

void test_middleDigitsOrSymbols();

void test_consecUppercase();

void test_consecLowercase();

void test_consecDigits();

void tests();



#endif // PSFUNCTIONTESTS_H
