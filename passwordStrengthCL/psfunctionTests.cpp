#include "psfunctionTests.h"

void test_countUppercase() {
    assert( countUppercase("HO891LA") == 4 );
    assert( countUppercase("hola") == 0 );
    assert( countUppercase("hola HOLA") == 4 );
    assert( countUppercase("") == 0 );
}

void test_countLowercase() {
    assert( countLowercase("HO891LA") == 0 );
    assert( countLowercase("@hola@") == 4 );
    assert( countLowercase("hello HOLA") == 5 );
    assert( countLowercase("") == 0 );
}

void test_countDigits() {
    assert( countDigits("HO891LA") == 3 );
    assert( countDigits("@hola@") == 0 );
    assert( countDigits("hello HOLA") == 0 );
    assert( countDigits("1") == 1 );
}

void test_countSymbols() {
    assert( countSymbols("HO891LA") == 0 );
    assert( countSymbols("@hola!") == 2 );
    assert( countSymbols("hello HOLA") == 1 );
    assert( countSymbols("A") == 0 );
}

void test_middleDigitsOrSymbols() {
    assert( middleDigitsOrSymbols("HO891LA") == 3 );
    assert( middleDigitsOrSymbols("@hola!") == 0 );
    assert( middleDigitsOrSymbols("hello!@#HOLA") == 3 );
    assert( middleDigitsOrSymbols("4@5") == 1 );
    assert( middleDigitsOrSymbols("") == 0 );
}

void test_consecUppercase() {
    assert( consecUppercase("") == 0);
    assert( consecUppercase("A") == 0);
    assert( consecUppercase("AaA") == 0);
    assert( consecUppercase("ADG5ZXCV") == 5);
}

void test_consecLowercase() {
    assert( consecLowercase("") == 0);
    assert( consecLowercase("b") == 0);
    assert( consecLowercase("a!w") == 0);
    assert( consecLowercase("Abanico!") == 5);
}

void test_consecDigits() {
    assert( consecDigits("") == 0);
    assert( consecDigits("b") == 0);
    assert( consecDigits("a!w") == 0);
    assert( consecDigits("Abanico!") == 0);
    assert( consecDigits("787!396!") == 4);
}

void tests() {
    test_countUppercase();
    test_countLowercase();
    test_countDigits();
    test_countSymbols();
    test_middleDigitsOrSymbols();
    test_consecUppercase();
    test_consecLowercase();
    test_consecDigits();
}

