#ifndef PSFUNCTIONS_H
#define PSFUNCTIONS_H

#include <iostream>
#include <string>

using namespace std;


string toUpperString(const string &st);

unsigned int countCharsOfType(const string &st,
    int (* filterFunction)(int args) ,
    unsigned int fromIdx, unsigned int toIdx);

unsigned int countUppercase(const string &st);

unsigned int countLowercase(const string &st);

unsigned int countDigits(const string &st);

unsigned int countSymbols(const string &st);

unsigned int middleDigitsOrSymbols(const string &st);

int isSymbol(int c);

int isDigitOrSymbol(int c);

unsigned int countConsecutive(const string &st, int (* filterFunction)(int args) );

unsigned int consecUppercase(const string &st);

unsigned int consecLowercase(const string &st);

unsigned int consecDigits(const string &st);


#endif // PSFUNCTIONS_H
