TEMPLATE = app
CONFIG += console
CONFIG += c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    psfunctions.cpp \
    psfunctionTests.cpp

HEADERS += \
    psfunctions.h \
    psfunctionTests.h

